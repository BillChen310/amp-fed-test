# How to run:

- Install node.js, npm, bower and karma-cli globally
- Go to project root folder
- Run bower (bower install)
- Run npm (npm install)
- Run Gruntfile by "Grunt"

## The expected result is:

A local server is started and the page is displayed, person can be searched by first name, last name or title.
