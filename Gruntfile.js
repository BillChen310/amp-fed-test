module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',

        sass: {
            dist: {
                files: {
                    'build/css/site.css': 'app/scss/site.scss'
                }
            }
        },

        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            },
            karma: {
                // run these tasks when these files change
                files: ['app/app.js', 'test/*.js'],
                tasks: ['karma:continuous:run'] // note the :run flag
            }
        },

        copy: {
            root: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['data.js', 'index.html'],
                    dest: 'build/'
                }]
            },
            app: {
                files: [{
                    expand: true,
                    cwd: 'app/',
                    src: ['*.js', '**/*.js'],
                    dest: 'build/'
                }]
            },
            img: {
                files: [{
                    expand: true,
                    cwd: 'img/',
                    src: ['*'],
                    dest: 'build/img/'
                }]
            },
            lib: {
                files: [{
                    expand: true,
                    cwd: 'bower_components/angular/',
                    src: ['*.min.js'],
                    dest: 'build/lib/'
                }]
            }
        },

        karma: {
            options: {
                configFile: 'karma.conf.js',
            },
            unit: {
                singleRun: true
            },
            continuous: {
                background: true
            }
        },

        'http-server': {
            'root': {
                root: "build/",
                host: "127.0.0.1",
                port: function(){ return 8080; },
                https: false,
                openBrowser: true,
                customPages: {
                    "/custom": "README.md"
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-http-server');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('dev', ['default', 'karma:continuous:start', 'watch']);
    grunt.registerTask('default', ['sass', 'copy','http-server']);

};