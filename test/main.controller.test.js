describe('searchPerson', function () {

    beforeEach(module('personApp'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('searchByInput', function () {
        var $scope, controller;

        beforeEach(function() {
            $scope = {};
            controller = $controller('mainController', { $scope: $scope });
        });

        it('search one letter matching case', function () {
            $scope.name = 'a';
            var expectedResult = [
                                    [
                                        {
                                            'id': '1',
                                            'firstName': 'Sean',
                                            'lastName': 'Kerr',
                                            'picture': 'img/sean.png',
                                            'Title': 'Senior Developer'
                                        },
                                        {
                                            'id': '2',
                                            'firstName': 'Yaw',
                                            'lastName': 'Ly',
                                            'picture': 'img/yaw.png',
                                            'Title': 'AEM Magician'
                                        },
                                        {
                                            'id': '3',
                                            'firstName': 'Lucy',
                                            'lastName': 'Hehir',
                                            'picture': 'img/lucy.png',
                                            'Title': 'Scrum master'
                                        }
                                    ],
                                    [
                                        {
                                            'id': '4',
                                            'firstName': 'Rory',
                                            'lastName': 'Elrick',
                                            'picture': 'img/rory.png',
                                            'Title': 'AEM Guru'
                                        },
                                        {
                                            'id': '5',
                                            'firstName': 'Eric',
                                            'lastName': 'Kwok',
                                            'picture': 'img/eric.png',
                                            'Title': 'Technical Lead'
                                        },
                                        {
                                            'id': '6',
                                            'firstName': 'Hayley',
                                            'lastName': 'Crimmins',
                                            'picture': 'img/hayley.png',
                                            'Title': 'Dev Manager'
                                        }
                                    ]
                                ];
            $scope.filterPersons();
            expect($scope.groupedPersons).toEqual(expectedResult);
        });

        it('search two letters not matching case', function () {
            $scope.name = 'lu';
            var expectedResult = [
                [
                    {
                        'id': '3',
                        'firstName': 'Lucy',
                        'lastName': 'Hehir',
                        'picture': 'img/lucy.png',
                        'Title': 'Scrum master'
                    }
                ]
            ];
            $scope.filterPersons();
            expect($scope.groupedPersons).toEqual(expectedResult);
        });

        it('search first name not matching case', function () {
            $scope.name = 'eric';
            var expectedResult = [
                [
                    {
                        'id': '5',
                        'firstName': 'Eric',
                        'lastName': 'Kwok',
                        'picture': 'img/eric.png',
                        'Title': 'Technical Lead'
                    }
                ]
            ];
            $scope.filterPersons();
            expect($scope.groupedPersons).toEqual(expectedResult);
        });

        it('search by empty text', function () {
            $scope.name = '';
            var expectedResult = [
                [
                    {
                        'id': '1',
                        'firstName': 'Sean',
                        'lastName': 'Kerr',
                        'picture': 'img/sean.png',
                        'Title': 'Senior Developer'
                    },
                    {
                        'id': '2',
                        'firstName': 'Yaw',
                        'lastName': 'Ly',
                        'picture': 'img/yaw.png',
                        'Title': 'AEM Magician'
                    },
                    {
                        'id': '3',
                        'firstName': 'Lucy',
                        'lastName': 'Hehir',
                        'picture': 'img/lucy.png',
                        'Title': 'Scrum master'
                    }
                ],
                [
                    {
                        'id': '4',
                        'firstName': 'Rory',
                        'lastName': 'Elrick',
                        'picture': 'img/rory.png',
                        'Title': 'AEM Guru'
                    },
                    {
                        'id': '5',
                        'firstName': 'Eric',
                        'lastName': 'Kwok',
                        'picture': 'img/eric.png',
                        'Title': 'Technical Lead'
                    },
                    {
                        'id': '6',
                        'firstName': 'Hayley',
                        'lastName': 'Crimmins',
                        'picture': 'img/hayley.png',
                        'Title': 'Dev Manager'
                    }
                ]
            ];
            $scope.filterPersons();
            expect($scope.groupedPersons).toEqual(expectedResult);
        });
    });

});