(function () {
    'use strict';

    angular.module('personApp.services').service('commonService', [function () {
        var personAPI = {};

        personAPI.searchPersons = function(searchText, personList) {
            var matchReg = new RegExp(searchText, "i");
            var filteredPersons = [];
            personList.forEach(function(person){
                var fullName = '';
                if (person.firstName && person.lastName) {
                    fullName = person.firstName + ' ' + person.lastName;
                }
                if (fullName.match(matchReg) || person.Title.match(matchReg)) {
                    filteredPersons.push(person);
                }
            });
            return filteredPersons;
        }

        personAPI.createPersonGroups = function(personList, numbersForEachRow) {
            var index = 0;
            var rowCount = 0;
            var groupedPersons = [];
            personList.forEach(function (person) {
                var newRowCount = Math.floor(index / numbersForEachRow) + 1;
                if (newRowCount > rowCount) {
                    var newGroup = [person];
                    groupedPersons.push(newGroup);
                    rowCount = newRowCount;
                } else {
                    var lastGroup = groupedPersons[groupedPersons.length - 1];
                    lastGroup.push(person);
                }
                index++;
            });

            return groupedPersons;
        }

        return personAPI;
    }]);

}());
