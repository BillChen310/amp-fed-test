angular.module('personApp', ['personApp.directives', 'personApp.services'])
.controller('mainController', ['$scope', 'commonService', function($scope, commonService) {
    $scope.numbersForEachRow = 3;
    $scope.persons = angular.fromJson(data);
    $scope.groupedPersons = commonService.createPersonGroups($scope.persons, $scope.numbersForEachRow);

    $scope.filterPersons = function() {
        $scope.groupedPersons = [];
        var filteredPersons = commonService.searchPersons($scope.name, $scope.persons);
        if (filteredPersons.length > 0) {
            $scope.groupedPersons = commonService.createPersonGroups(filteredPersons, $scope.numbersForEachRow);
        } 
    }
}]);

angular.module('personApp.directives', []);
angular.module('personApp.services', []);
