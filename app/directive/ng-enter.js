(function () {
    'use strict';
    angular.module('personApp.directives')
        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keyup", function (event) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                });
            };
        });
}());

